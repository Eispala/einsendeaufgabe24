﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

using Windows.Storage;

// Die Elementvorlage "Leere Seite" wird unter https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x407 dokumentiert.

namespace TextEdit
{
    /// <summary>
    /// Eine leere Seite, die eigenständig verwendet oder zu der innerhalb eines Rahmens navigiert werden kann.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
            this.DataContext = this;
            SetFontSize();
        }

        public string lblText = "";

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            _ = tbText.FontSize < 40 ? tbText.FontSize += 1 : tbText.FontSize = tbText.FontSize;
            SetFontSize();

        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            _ = tbText.FontSize > 10 ? tbText.FontSize -= 1 : tbText.FontSize = tbText.FontSize;
            SetFontSize();
        }

        private void SetFontSize()
        {

            lblText = $"Schriftgröße: {Convert.ToInt32(tbText.FontSize)}";
            lblFontSize.Text = lblText;
        }

        public void SaveFontSize()
        {
            ApplicationDataContainer dataContainer = ApplicationData.Current.LocalSettings;
            dataContainer.Values["fontSize"] = tbText.FontSize;
            string sText = "";
            tbText.Document.GetText(Windows.UI.Text.TextGetOptions.None, out sText);
            dataContainer.Values["text"] = sText;
        }

        public void ReadFontSize()
        {
            ApplicationDataContainer dataContainer = ApplicationData.Current.LocalSettings;
            if(dataContainer.Values.ContainsKey("fontSize"))
            {
                tbText.FontSize = (double)dataContainer.Values["fontSize"];
                tbText.Document.SetText(Windows.UI.Text.TextSetOptions.None, dataContainer.Values["text"].ToString());
                SetFontSize();
            }
        }

    }
}
